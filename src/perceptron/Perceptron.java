
package perceptron;


public class Perceptron {

    double [] enters;//wchody
    double outer; //wychody
    
    //udobnee dwunerny massiw no tut
    //malo wesowych koficintow, synopsy, wesy)dwumer 1 kof - od kakogo , 2 - k kakomu nierronu)
    
    double [] weights;
    
    double[][] patterns_or1={
        {0,0,0},
        {0,1,1},
        {1,0,1},
        {1,1,1}
    };
    
    double[][] patterns_or={
        {0,0,0},
        {0,1,0},
        {1,0,0},
        {1,1,1}
    };
    
    double[][] patterns_not={
        {1,0},
        {0,1}
    };
    
    
    public Perceptron(){
        enters = new double[2];
        weights = new double[enters.length]; // priwiazka koloczestwa wesow k koliczstwu nuironow
        for(int i=0; i<enters.length; i++)//zadajm wesy
        {
            weights[i]=Math.random()*0.2+0.1;
            
        }
    }
    
    //metod wyczeslenia wychoda perceptrona
    public void CounToOut()
    {
        outer=0;
        for(int i=0;i<enters.length;i++)
        {
            outer+=enters[i]*weights[i];
        }
        if(outer>0.5)
        {
            outer=1;
        }
        else
        {
            outer=0;
        }
        
    }    
    
    public void Study()
    {
        double global_error = 0;
        do
        {
            global_error=0;
            for(int p=0;p<patterns_or.length;p++)
            {
                enters = java.util.Arrays.copyOf(patterns_or[p], patterns_or[p].length-1);
                CounToOut();
                double error = patterns_or[p][2]-outer;
                global_error+=Math.abs(error);
                for(int i =0; i<enters.length; i++)
                {
                    weights[i]+=0.1*error*enters[i];
                }
            }
        }while(global_error!=0);
    }
    
    public void Test()
    {
        Study();
         for(int p=0;p<patterns_or.length;p++)
            {
                enters = java.util.Arrays.copyOf(patterns_or[p], patterns_or[p].length-1);
                CounToOut();               
                System.out.println(p+1 + ":" +  outer);
            }
        
    }
    public static void main(String[] args) 
    {
       new Perceptron().Test();
    }
    
}
